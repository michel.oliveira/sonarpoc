﻿using System;

namespace SonarTest
{
    public class Class1
    {
        public bool IsPrime(int candidate)
        {
            int y = 0;
            string a = "";
            int x = 1;
            int z = 1;

            if (candidate < 2)
            {
                return false;
            }

            for (var divisor = 2; divisor <= Math.Sqrt(candidate); divisor++)
            {
                if (candidate % divisor == 0)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
